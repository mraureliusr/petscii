#!/usr/bin/env python3

import sys

def convert(byte):
    if byte == 13: # fix line endings
        return 10
    if byte == 63: # don't change question marks
        return byte
    if byte >= 97 and byte <= 122:
        return byte - 32
    if byte >= 65 and byte <= 90:
        return byte + 32
    if byte >= 192 and byte <= 223:
        byte -= 96
        if byte >= 97 and byte <= 122:
            byte -= 32
    return byte

def main():
    filename = sys.argv[1]
    with open(filename, "rb") as f:
        while (byte := f.read(1)):
            byte = convert(int.from_bytes(byte))
            print(byte.to_bytes(1).decode('ascii'), end="")

if __name__ == "__main__":
    main()