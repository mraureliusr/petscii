# PETSCII and WordWriter 4 Converters

These two Python scripts are just simple conversion scripts that convert from PETSCII (at least, the PETSCII that Turbo Macro Pro puts out, which is the lowercase character set) to ASCII. While working on that project, I also needed to convert from the format WordWriter 4 for the C64 saves into ASCII. That is what the second script does.

## Usage

In both cases, simply run the script followed by the filename:

```
./petscii.py document > document_converted
```

```
./wordwriter.py document > document_converted
```

Make sure the .py files have +x permission (run chmod +x on them if needed) or you can just call them with python3:

```
python3 wordwriter.py document
```

## Bugs

Since I only needed to convert fairly simple files, the conversions might not convert every single document. In particular, a lot of the symbols like &, % etc. won't be converted by wordwriter.py I don't think. The petscii.py should convert most things. If you hit a snag, just post an issue.
