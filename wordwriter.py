#!/usr/bin/env python3

import sys

def convert(byte):
    if byte < 0x20: # lowercase
        byte = byte | 0x60
    if byte == 0x60:
        byte = 0x40
    if byte == 0xFE or byte == 0xFF: # line feed
        byte = 0x0A
    if byte > 0x7F: # control codes
        byte = 0x20
    return byte

def main():
    filename = sys.argv[1]
    with open(filename, "rb") as f:
        while (byte := f.read(1)):
            if int.from_bytes(byte) == 0x00: # skip all the bytes before the first 0
                break
        while (byte := f.read(1)):
            byte = convert(int.from_bytes(byte))

            print(byte.to_bytes(1).decode('ascii'), end="")

if __name__ == "__main__":
    main()